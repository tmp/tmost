use std::path::PathBuf;
use structopt::clap::AppSettings;
use structopt::StructOpt;

use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use termion::screen::AlternateScreen;

mod apps;
use crate::apps::common::InputEvent;
use crate::apps::common::Pause;
use crate::apps::stopwatch::Stopwatch;
use crate::apps::timer::Timer;
use dirs;
use humantime::parse_duration;
use std::io::{stdin, stdout, Write};
use std::process;
use std::sync::mpsc;
use std::time::Duration;
use std::{fmt, thread}; // TODO: use lightweight/green threads instead: async/await or tokio; OR use coroutine crate!

#[derive(StructOpt, Debug)]
#[structopt(raw(setting = "AppSettings::InferSubcommands"))]
#[structopt(name = "tmost")]
/// Time utilities including timer, stopwatch, and pomodoro timer
enum Tmost {
    #[structopt(name = "timer")]
    /// Timer
    Timer {
        #[structopt(name = "message", short, long, default_value = "")]
        message: String,
        #[structopt(
            name = "countdown",
            required = true,
            help = "Duration of timer, e.g. '1min', '2h30m', etc.",
            parse(try_from_str = "parse_duration")
        )]
        countdown: Duration,
        #[structopt(
            name = "hook",
            help = "Path to a hook script. Default: '$CONFIG/tmost/exec/hook'",
            long,
            parse(from_os_str)
        )]
        hook: Option<PathBuf>,
    },
    #[structopt(name = "stopwatch")]
    /// Stop the time elapsed
    Stopwatch {
        #[structopt(name = "message", short, long)]
        message: String,
        #[structopt(
            name = "hook",
            help = "Path to a hook script. Default: '$config/tmost/exec/hook'",
            long,
            parse(from_os_str)
        )]
        hook: Option<PathBuf>,
    },
    #[structopt(name = "pomodoro")]
    /// Pomodoro timer
    Pomodoro {
        #[structopt(name = "message", short, long, default_value = "")]
        message: String,
    },
}

fn print_loop<S: fmt::Display + Pause>(
    stdout: termion::raw::RawTerminal<std::io::Stdout>,
    output_rx: mpsc::Receiver<Box<S>>,
) {
    let mut screen = AlternateScreen::from(stdout);

    write!(screen, "{}", termion::clear::All).expect("write!() failed");

    let refresh_time_default = Duration::new(0, 10_000_000); // 1/10 s
    let refresh_time_paused = Duration::new(0, 500_000_000); // 1/2 s
    let mut sleep = refresh_time_default;

    // When app_state is paused, this is used for blinking
    let mut blank = false;

    let mut shutdown = false;

    // Get first app instance
    let mut app_state = output_rx.recv().unwrap();
    loop {
        // Compute next sleep duration and advance timer
        match output_rx.recv_timeout(sleep) {
            Ok(app_state_update) => {
                app_state = app_state_update;
            }
            Err(mpsc::RecvTimeoutError::Timeout) => (),
            //Err(mpsc::RecvError) => break,
            _ => shutdown = true, // TODO: Get Error when channel is closed
        }
        if app_state.is_paused() {
            sleep = refresh_time_paused;
            blank = !blank; // Toggle pause blinking
        } else {
            sleep = refresh_time_default;
            blank = false;
        }
        // Printing
        if blank {
            // pause blank
            write!(
                screen,
                "{}", // clear All
                termion::clear::All,
            )
            .expect("write!() failed");
        } else {
            // running
            write!(screen, "{}{}", termion::cursor::Goto(1, 1), app_state,)
                .expect("write!() failed");
        }
        screen.flush().unwrap();
        if shutdown {
            break;
        }
    }
}

fn hook_or_default(hook: Option<PathBuf>) -> Option<PathBuf> {
    if hook.is_none() {
        // Check if default hook exists:
        let mut default_hook = dirs::config_dir().unwrap();
        default_hook.push(PathBuf::from("tmost/exec/hook"));
        if default_hook.exists() {
            return Some(default_hook);
        }
    }
    hook
}

fn main() {
    let stdin = stdin();
    let (input_tx, input_rx) = mpsc::channel(); // For sending keyboard input events to the app
    let _input_loop = thread::spawn(move || {
        for c in stdin.keys() {
            match c.unwrap() {
                Key::Char('\n') => {
                    input_tx
                        .send(InputEvent::Lap)
                        .expect("failed to send event to app thread");
                }
                Key::Char(' ') => {
                    input_tx
                        .send(InputEvent::TogglePause)
                        .expect("failed to send event to app thread");
                }
                Key::Char('q') | Key::Ctrl('c') | Key::Esc => {
                    input_tx
                        .send(InputEvent::Exit)
                        .expect("failed to send event to app thread");
                    break;
                }
                _ => (), // Ignore unspecified keys
            };
        }
    });

    let args = Tmost::from_args();
    match args {
        Tmost::Timer {
            message,
            countdown,
            hook,
        } => {
            let hook = hook_or_default(hook);
            let (output_tx, output_rx) = mpsc::channel(); // for sending app events to print function
            let timer = Timer::new(message, countdown, hook);
            let app_thread = thread::spawn(move || {
                timer.run(input_rx, output_tx);
            });
            let stdout = stdout().into_raw_mode().unwrap();
            print_loop(stdout, output_rx);
            let _ = app_thread.join();
        }
        Tmost::Stopwatch { message, hook } => {
            let hook = hook_or_default(hook);
            let (output_tx, output_rx) = mpsc::channel(); // for sending app events to print function
            let stop_watch = Stopwatch::new(message, hook);
            let app_thread = thread::spawn(move || {
                stop_watch.run(input_rx, output_tx);
            });
            let stdout = stdout().into_raw_mode().unwrap();
            print_loop(stdout, output_rx);
            let _ = app_thread.join();
        }
        Tmost::Pomodoro { message } => {
            println!("{}", message);
            println!("Pomodoro not implemented");
            process::exit(1);
        }
    }
}
