use std::fmt;
use std::path::PathBuf;
use std::process::Command;
use std::sync::mpsc;
use std::time::{Duration, Instant};

use crate::apps::common::{InputEvent, Pause};

#[derive(Clone, Debug)]
pub struct Timer {
    message: String,
    hook: Option<PathBuf>,
    start_time: Option<Instant>,
    duration: Duration,
    pause_time: Option<Instant>,
}

impl Timer {
    pub fn new(message: String, duration: Duration, hook: Option<PathBuf>) -> Timer {
        Timer {
            message,
            hook,
            start_time: None,
            duration,
            pause_time: None,
        }
    }

    fn hook(&self, event: String) {
        match &self.hook {
            Some(hook) => match Command::new(hook)
                .arg("timer")
                .arg(event)
                .arg(&self.message)
                .status()
            {
                Ok(_) => (),
                Err(e) => eprintln!("Failed to execute hook script '{}': {}", hook.display(), e),
            },
            None => (),
        }
    }

    pub fn run(
        mut self,
        input_rx: mpsc::Receiver<InputEvent>,
        output_tx: mpsc::Sender<Box<Timer>>,
    ) {
        // Notify others that we start running
        self.hook(String::from("start"));

        self.start_time = Some(Instant::now());
        output_tx.send(Box::new(self.clone())).unwrap();

        let mut sleep: Duration;
        loop {
            if self.is_paused() {
                sleep = Duration::new(std::u64::MAX, 0)
            } else {
                sleep = self.remaining();
            }
            match input_rx.recv_timeout(sleep) {
                Ok(InputEvent::Exit) => {
                    self.hook(String::from("exit"));
                    break;
                }
                Ok(InputEvent::TogglePause) => {
                    if self.is_paused() {
                        self.start_time = Some(
                            self.start_time.unwrap()
                                + Instant::now().duration_since(self.pause_time.unwrap()),
                        );
                        self.pause_time = None;
                        output_tx.send(Box::new(self.clone())).unwrap();
                        self.hook(String::from("resume"));
                    } else {
                        // timer currently running
                        self.pause_time = Some(Instant::now());
                        output_tx.send(Box::new(self.clone())).unwrap();
                        self.hook(String::from("pause"));
                    }
                }
                Err(mpsc::RecvTimeoutError::Timeout) => {
                    if !self.is_paused() {
                        self.hook(String::from("finish"));
                        break;
                    }
                }
                _ => (), // print error message because of unknown command?
            }
        }
    }

    // fn elapsed(&self) -> Duration {
    //     if self.is_paused() {
    //         let now = Instant::now();
    //         now.duration_since(self.start_time.unwrap())
    //             - now.duration_since(self.pause_time.unwrap())
    //     } else {
    //         self.start_time.unwrap().elapsed()
    //     }
    // }

    fn remaining(&self) -> Duration {
        let remaining = if self.is_paused() {
            let now = Instant::now();
            self.duration.checked_sub(
                now.duration_since(self.start_time.unwrap())
                    - now.duration_since(self.pause_time.unwrap()),
            )
        } else {
            self.duration
                .checked_sub(self.start_time.unwrap().elapsed())
        };
        match remaining {
            Some(r) => r,
            None => Duration::new(0, 0),
        }
    }
}

impl fmt::Display for Timer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let remaining = self.remaining();
        write!(
            f,
            "{}\r\n{:2}h {:2}m {:2}.{}s",
            self.message,
            remaining.as_secs() / 3600,             // hours
            (remaining.as_secs() / 60) % 60,        // minutes
            remaining.as_secs() % 60,               // seconds
            remaining.subsec_nanos() / 100_000_000, // 1/10 of a second
        )
    }
}

impl Pause for Timer {
    fn is_paused(&self) -> bool {
        self.pause_time.is_some()
    }
}
