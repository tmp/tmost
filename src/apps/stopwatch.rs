use std::fmt;
use std::path::PathBuf;
use std::process::Command;
use std::sync::mpsc;
use std::time::{Duration, Instant};

use crate::apps::common::{InputEvent, Pause};

#[derive(Clone, Debug)]
pub struct Stopwatch {
    message: String,
    hook: Option<PathBuf>,
    start_time: Option<Instant>,
    pause_time: Option<Instant>,
}

impl Stopwatch {
    pub fn new(message: String, hook: Option<PathBuf>) -> Stopwatch {
        Stopwatch {
            message,
            hook,
            start_time: None,
            pause_time: None,
        }
    }

    fn hook(&self, event: String) {
        match &self.hook {
            Some(hook) => match Command::new(hook)
                .arg("stopwatch")
                .arg(event)
                .arg(&self.message)
                .status()
            {
                Ok(_) => (),
                Err(e) => eprintln!("Failed to execute hook script '{}': {}", hook.display(), e),
            },
            None => (),
        }
    }

    pub fn run(
        mut self,
        input_rx: mpsc::Receiver<InputEvent>,
        output_tx: mpsc::Sender<Box<Stopwatch>>,
    ) {
        // Notify others that we start running
        self.hook(String::from("start"));

        self.start_time = Some(Instant::now());
        output_tx.send(Box::new(self.clone())).unwrap();

        loop {
            match input_rx.recv() {
                Ok(InputEvent::Exit) => {
                    self.hook(String::from("exit"));
                    break;
                }
                Ok(InputEvent::TogglePause) => {
                    if self.is_paused() {
                        self.start_time = Some(
                            self.start_time.unwrap()
                                + Instant::now().duration_since(self.pause_time.unwrap()),
                        );
                        self.pause_time = None;
                        output_tx.send(Box::new(self.clone())).unwrap();
                        self.hook(String::from("resume"));
                    } else {
                        // timer currently running
                        self.pause_time = Some(Instant::now());
                        output_tx.send(Box::new(self.clone())).unwrap();
                        self.hook(String::from("pause"));
                    }
                }
                _ => (), // print error message because of unknown command?
            }
        }
    }

    fn elapsed(&self) -> Duration {
        if self.is_paused() {
            let now = Instant::now();
            now.duration_since(self.start_time.unwrap())
                - now.duration_since(self.pause_time.unwrap())
        } else {
            self.start_time.unwrap().elapsed()
        }
    }
}

impl Pause for Stopwatch {
    fn is_paused(&self) -> bool {
        self.pause_time.is_some()
    }
}

impl fmt::Display for Stopwatch {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let elapsed = self.elapsed();
        write!(
            f,
            "{}\r\n{:2}h {:2}m {:2}.{}s",
            self.message,
            elapsed.as_secs() / 3600,             // hours
            (elapsed.as_secs() / 60) % 60,        // minutes
            elapsed.as_secs() % 60,               // seconds
            elapsed.subsec_nanos() / 100_000_000, // 1/10 of a second
        )
    }
}
