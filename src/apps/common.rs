pub enum InputEvent {
    // WakeUp, // notify
    TogglePause,
    Lap, // not implemented
    Exit,
}

pub trait Pause {
    fn is_paused(&self) -> bool;
}
