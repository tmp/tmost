# tmost

Interactive terminal time tools: timer, stopwatch, pomodoro, ...


## Compile and install
Install Rust: [Instructions](https://www.rust-lang.org/tools/install).

Build `tmost`:
```bash
git clone https://codeberg.org/tmp/tmost.git
cd tmost

# Compile and install
cargo install --path . --force

# Or build and run binary w/o installation
cargo build --release
./target/release/tmost
```

## Usage

```
tmost timer <duration>
```

```
tmost stopwatch
```

### Keys

- space / p = pause
- q / esc / ctl-c = quit
- enter = lap
- n = next (pomodoro)
- r = reset
- ? / h = help
